# Musterlösung zum Arbeitsblatt WS02-Plugin

Dieses Projekt enthält eine Beispiel-Implementierung zur dynamischen Instanzierung 
von unbekannten Datentypen aus Basis von Interface-Definitionen.

Im konkreten Beispiel können verschiedene Rechner-Implementierungen (z.B. ScientificCalculator) 
auf der Basis eines generischen Calculator-Interfaces dynamisch geladen und zur 
Ausführung gebracht werden, ohne dass die ScientificCalculator Assembly dem Programm bekannt
ist. 

Der zu ladende und auszuführende konkrete Calculator wird als Argument dem Programm übergeben.
