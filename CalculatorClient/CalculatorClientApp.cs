﻿using System;
using System.Reflection;
using Calculators;
using System.Linq;

namespace DynamicInvocation
{
    /// <summary>
    /// Demonstrates dynamic creation of types during runtime.
    /// Uses a ICalculator interface and an implementation(SimpleCalculator), which is 
    /// dynamically loaded and executed.
    /// </summary>
    class CalculatorClientApp
    {
        private static Assembly assembly;

        static void Main(string[] args)
        {
            if (args == null)
            {
                throw new ArgumentException("no calculator provided");
            }

            // try to get the type
            Type type = GetTypeFromAssembly(args[0]);

            // invoke method through generic "Invoke" method
            InvokeMethodDynamic(type);

            // Invoke method through interface
            InvokeMethodByInterface(type);

            Console.ReadKey();
        }



        /// <summary>
        /// Gets the first type in the assembly, which implements the ICalculator interface.
        /// </summary>
        private static Type GetTypeFromAssembly(string assemblyName)
        {
            assembly = Assembly.LoadFrom(assemblyName);

            // use LINQ to find the type who implements the correct interface
            return assembly
                .GetTypes()
                .Where(t => t.IsClass)
                .Single(t => t.GetInterface("ICalculator")!=null);
// another solution                .Single(t => typeof(ICalculator).IsAssignableFrom(t));

        }



        /// <summary>
        /// the method provides multiple solutions for the invocation
        /// of the Add-method of the passed in type. It is assumed the type
        /// implements the ICalculator Interface.
        /// </summary>
        /// <param name="type"></param>
        private static void InvokeMethodDynamic(Type type)
        {
            object calcObj = assembly.CreateInstance(type.FullName);

            // Solution 1: getMethod and Invoke
            MethodInfo method = type.GetMethod("Add");
            var arguments = new object[] { 10d, 20d };
            object res1 = method.Invoke(calcObj, arguments);
            double sum1 = (double)res1;
            Console.WriteLine($"Solution 1: {type.Name}.Add(10, 20) = {sum1}");


            // Solution 2: direct invocation through InvokeMember
            object res2 = type.InvokeMember("Add",
                                           BindingFlags.InvokeMethod,
                                           null,
                                           calcObj,
                                           arguments);
            var sum2 = (double)res2;
            Console.WriteLine($"Solution 2: {type.Name}.Add(10, 20) = {sum2}");


            // Solution 3: use DLR
            double sum3 = ((dynamic)calcObj).Add(10d, 20d);
            Console.WriteLine($"Solution 3: {type.Name}.Add(10, 20) = {sum3}");
        }

        /// <summary>
        /// This method provides the invocation of the Add-method
        /// based in the ICalculator Interface, 
        /// </summary>
        /// <param name="type"></param>
        private static void InvokeMethodByInterface(Type type)
        {
            // create instance 
            ICalculator calcObj =
                assembly.CreateInstance(type.FullName) as ICalculator;

            // call method
            double sum = calcObj.Add(10d, 20d);

            Console.WriteLine($"Solution 4: {type.Name}.Add(10, 20) = "
                + sum);
        }

    }
}
